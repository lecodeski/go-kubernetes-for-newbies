---
- name: Check if server is ready
  stat:
    path: /opt/ready.txt
  register: ready

- fail:
    msg: "Server is not ready"
  when: not ready.stat.exists

- name: Configure floating IPs
  template:
    src: 60-floating-ip.j2
    dest: /etc/network/interfaces.d/60-floating-ip.cfg

- name: Restart networking
  shell: systemctl restart networking.service

- name: Copy Hetzner Cloud config provider flag for kubeadm
  copy:
    src: "{{ playbook_dir }}/roles/kube-prepare/files/20-hetzner-cloud.conf"
    dest: /etc/systemd/system/kubelet.service.d/20-hetzner-cloud.conf
    mode: u=rw,g=r,o=r

- name: Create Docker service directory if it does not exist yet
  file:
    path: /etc/systemd/system/docker.service.d
    state: directory

- name: Copy Docker config for systemd
  copy:
    src: "{{ playbook_dir }}/roles/kube-prepare/files/00-cgroup-systemd.conf"
    dest: /etc/systemd/system/docker.service.d/00-cgroup-systemd.conf
    mode: u=rw,g=r,o=r

- name: Reload system daemon
  shell: systemctl daemon-reload

- name: Reload docker daemon
  shell: systemctl restart docker.service

- name: Insert or update sysctl settings for IPv4 traffic forwarding
  blockinfile:
    path: /etc/sysctl.conf
    block: |
      # Allow IP forwarding for Kubernetes
      net.bridge.bridge-nf-call-iptables = 1
      net.ipv4.ip_forward = 1

- name: Reload sysctl
  shell: sysctl -p

- name: Mount BPF filesystem for Kubernetes cilium network plugin
  mount:
    src: bpffs
    path: /sys/fs/bpf
    fstype: bpf
    opts: defaults 0 0
    state: mounted

- name: Mark servers prepared
  file:
    path: /opt/prepared.txt
    state: touch
